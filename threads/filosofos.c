/*Comando para compilar no GCC:
$ gcc -o filosofos filosofos.c -pthread*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define QUANT		(10)									//Quantidade de filósofos
#define ESQUERDA	(id_filosofo + QUANT - 1) % QUANT	//Id do filósofo a esquerda do id
#define DIREITA		(id_filosofo + 1) % QUANT			//Id do filósofo a direita do id
#define PENSANDO	(0)									//Id para estado pensado
#define FAMINTO		(1)									//Id para estado de fome
#define COMENDO		(2)									//Id para estado comendo

int estado [QUANT];								//Estado dos filósofos
pthread_mutex_t mutex;							//Região crítica
pthread_mutex_t mux_filo [QUANT];				//Mutex por filósofo
pthread_t jantar[QUANT];						//Todos os filósofos

void *filosofo(void *param);
void pegar_garfo(int id_filosofo);
void devolver_garfo(int id_filosofo);
void intencao(int id_filosofo);
void comer(int id_filosofo);
void pensar(int id_filosofo);

void *filosofo(void *vparam)//funcao que simula um filosofo, vparam id do filosofo
{
    int *id = (int*)(vparam);//Repassa o id do filósofo
    printf("Id aqui eh %d\n",*(id) );
    printf("Filosofo %d foi criado com sucesso\n", *(id));

    while (1){
        pensar(*(id));			//Aguarda o filósofo pensar
        pegar_garfo(*(id));		//Filósofo pega os garfos
        comer(*(id));			//Aguarda comer
        devolver_garfo(*(id));	//Devolver os garfos pra mesa
    }

    pthread_exit((void*)0);		//Legado do retorno
}

void pegar_garfo(int id_filosofo) // funcao que filosofo pega os garfos da mesa, id_filosofo id do filosofo
{
    pthread_mutex_lock(&(mutex));					//Entra na regiсo crítica
    printf("Filosofo %d esta faminto\n", id_filosofo);
    estado[id_filosofo] = FAMINTO;				//Altera o estado do filósofo
    intencao(id_filosofo);						//Intenção de pegar os garfos
    pthread_mutex_unlock(&(mutex));				//Sai na região crítica
    pthread_mutex_lock(&(mux_filo[id_filosofo]));	//Bloqueia os garfos
}

void devolver_garfo(int id_filosofo)// funcao que filosofo devolve os garfos para a mesa, id_filosofo id do filosofo
{
    pthread_mutex_lock (&(mutex));	//Entra na regiсo crítica
    printf("Filosofo %d esta pensando\n", id_filosofo);
    estado[id_filosofo] = PENSANDO;	//Terminou de comer
    intencao(ESQUERDA);				//Vê se o vizinho da esquerda pode comer agora
    intencao(DIREITA);				//Vê se o vizinho da direita pode comer agora
    pthread_mutex_unlock(&(mutex));	//Sai da regiсo crítica
}

void intencao(int id_filosofo)//funcao que filosofo tem intencao de pegar os garfos para comer
{
    printf("Filosofo %d esta com intencao de comer\n", id_filosofo);
    if((estado[id_filosofo] == FAMINTO) &&	//Se o filósofo está come fome
       (estado[ESQUERDA] != COMENDO) &&	//Se o vizinho da esquerda não está comendo
       (estado[DIREITA] != COMENDO))		//Se o vizinho da direita nсo está comendo
    {
        printf("Filosofo %d ganhou a vez de comer\n", id_filosofo);
        estado[id_filosofo] = COMENDO;	//Filósofo ganho a vez de comer
        pthread_mutex_unlock(&(mux_filo[id_filosofo]));	//Libera os garfos
    }
}

void pensar(int id_filosofo)//funcao que filosofo fica pensando
{
    int r = (rand() % 10+1);

    printf("Filosofo %d pensa por %d segundos\n", id_filosofo, r);
    sleep(r);	//Gasta um tempo em segundos
}

void comer(int id_filosofo)//funcao que filosofo fica comendo
{
    int r = (rand() % 10+1);

    printf("Filosofo %d come por %d segundos\n", id_filosofo, r);
    sleep(r);	//Gasta um tempo em segundos

}

int main (){

    int cont;	//Contador auxiliar
    int status;	//Criação da thread

    //Inicia os mutexes
    pthread_mutex_init(&(mutex), NULL);
    for( cont = 0; cont < QUANT; cont++){
        pthread_mutex_init(&(mux_filo[cont]), NULL);
    }

    //Inicia threads (filósofos)
    for(cont = 0; cont < QUANT ; cont++)
    {
        int i=cont;
        //verifica se ocorreu erro
        status = pthread_create(&(jantar[cont]), NULL, filosofo, (void*) &(i));
        if (status)
        {
            printf("Erro criando thread %d, retornou codigo %d\n", cont, status);
            return EXIT_FAILURE;
        }
    }

    //Destroi antes de sair
    pthread_mutex_destroy(&(mutex));
    for( cont = 0; cont < QUANT; cont++){
        pthread_mutex_destroy(&(mux_filo[cont]));
    }
    pthread_exit(NULL);

    return EXIT_SUCCESS;
}